# HolyFSM

A CPU optimized event driven Finite State Machine with branches in
[rust-lang](http://www.rust-lang.org/).

**WIP**

Goals:
- Event-driven Finite State Machine
- Transitions can exit to different states using branches
- CPU optimized (not RAM optimized)
- No code generation (self-descripting FSM)
- Dot generation
- Sanity checks

This small project try to reproduce what have been done with
[angfsm](https://gitlab.com/mojo42/angfsm) using
[rust-lang](http://www.rust-lang.org/).

The idea is to try avoiding a second compilation using the powerfull Rust macro
system.

TODO:
- explain why FSM generation is evil
- explain why CPU optimized
- explain why branching is cool
