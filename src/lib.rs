// Copyright (c) 2016, Jérôme Jutteau <j.jutteau@gmail.com>
// All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// First simple event-driven FSM with branches.
// Now, we must wrap all this in a nice macro.
//
// +------>[S1]<-----+
// |        |        |
// |        |event1  |branch 2
// |        |        |
// |        +---->---+
// ^        |
// |        |branch 1
// |event2  |
// |        v
// +------[S2]

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum State {
    S1,
    S2,
    Invalid,
}
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Event {
    E1,
    E2,
}
#[derive(Debug, Copy, Clone)]
enum Branch {
    B1,
    B2,
    Default,
}

struct Transition {
    branches: [(Branch, State); 3],
    callback: fn() -> Branch,
}

impl Transition {
    fn empty() -> Transition {
        Transition {
            branches: [(Branch::B1, State::Invalid),
                       (Branch::B2, State::Invalid),
                       (Branch::Default, State::Invalid)],
            callback: StateMachine::trans_empty,
        }
    }
}

pub struct StateMachine {
    state: State,
}

impl Default for StateMachine {
    fn default() -> StateMachine {
        StateMachine { state: State::S1 }
    }
}

impl StateMachine {
    fn trans_s1_e1() -> Branch {
        println!("run transition trans_s1_e1()");
        Branch::B1 // or Branch::B2
    }

    fn trans_s2_e2() -> Branch {
        println!("run transition trans_s2_e2()");
        Branch::Default
    }

    fn trans_empty() -> Branch {
        panic!("bad transition should never been called");
    }

    pub fn run(&mut self, event: Event) {
        // Transition table
        //    |      S1     |      S2     |
        // ---+-------------+-------------+
        // E1 | trans_s1_e1 |    (none)   |
        // ---+-------------+-------------+
        // E2 |   (none)    | trans_s2_e2 |
        // ---+-------------+-------------+
        let transition_table = [[Transition {
                                     branches: [(Branch::B1, State::S2),
                                                (Branch::B2, State::S1),
                                                (Branch::Default, State::Invalid)],
                                     callback: StateMachine::trans_s1_e1,
                                 },
                                 Transition::empty()],
                                [Transition::empty(),
                                 Transition {
                                     branches: [(Branch::B1, State::Invalid),
                                                (Branch::B2, State::Invalid),
                                                (Branch::Default, State::S1)],
                                     callback: StateMachine::trans_s2_e2,
                                 }]];

        let ref transition = transition_table[self.state as usize][event as usize];
        if transition.callback == StateMachine::trans_empty {
            println!("event {:?} not managed", event);
            return;
        }

        let output_branch: Branch = (transition.callback)();
        let output_state: State = transition.branches[output_branch as usize].1;
        println!("[{:?}] --> [{:?}] --> {:?} ({:?})",
                 self.state,
                 event,
                 output_state,
                 output_branch);
        match output_state {
            State::Invalid => panic!("wrong branch {:?}", output_branch),
            valid_state => self.state = valid_state,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn it_works() {
        let mut machine = StateMachine::default();
        assert_eq!(machine.state, State::S1);
        machine.run(Event::E1);
        assert_eq!(machine.state, State::S2);
        machine.run(Event::E2);
        assert_eq!(machine.state, State::S1);
        machine.run(Event::E2);
        assert_eq!(machine.state, State::S1);
    }
}
